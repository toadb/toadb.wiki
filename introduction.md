# 简介

toad database system，“癞蛤蟆”数据库，正如其名，我们想吃天鹅肉，它是一款从零完全开始手写的数据库，同时在每做一步都会有教程和分析，期待更多人加入，一起同行。

# 教程地程 

地址：https://senllang.blog.csdn.net/category_12338586.html

介绍数据库整体架构，SQL执行流程，重点对于存储架构，SQL解析，SQL执行进行了重点分析； 根据代码的更新进度，详细介绍各模块流程，开发流程。

# 源码地址 

gitcode : https://gitcode.com/toadb/toadb/overview


![手写toadb.png](https://cdn-img.gitcode.com/da/de/d2ad39e495b30cf2b07666a7d4169ed2b65d8646d55ed739ad52dea012d99809.png '手写toadb.png')
